# cert-manager

Deployment to add a list of clusterIssuers to openshift-cern-cert-manager by the
https://gitlab.cern.ch/paas-tools/okd4-install/-/blob/master/chart/charts/cert-manager/templates/cert-manager-cluster-issuer.yaml application.

See https://gitlab.cern.ch/paas-tools/okd4-install/-/blob/master/docs/components/cert-manager/README.md
